extern crate piston_window;
use piston_window::*;

pub trait Shape
{
    fn new(color: [f32; 4]) -> Self;
    fn get_shape(&self, event: &Event, context: &Context, graphics: &mut G2d);
    fn change_color(&mut self, color: [f32; 4]);
    fn move_to(&mut self, x: f64, y: f64);
}

pub struct Mrectangle
{
    x: f64,
    y: f64,
    width: f64,
    height: f64, 

    color: [f32; 4]
}

impl Mrectangle
{
    pub fn new(x: f64, y: f64, width: f64, height: f64) -> Mrectangle {
        Mrectangle {
            color: [1.0, 0.0, 0.0, 1.0],
            x: x,
            y: y,
            width: width,
            height: height,
        }
    }
    pub fn transform(&mut self, width: f64, height: f64)
    {
        self.width = width;
        self.height = height;
    }
}

impl Shape for Mrectangle
{
    fn new(color: [f32; 4]) -> Mrectangle {
        let rect = Mrectangle {
            color: color,
            x: 0.0,
            y: 0.0,
            width: 0.0,
            height: 0.0,
        };
        return rect;
    }
    fn get_shape(&self, event: &Event, context: &Context, graphics: &mut G2d)
    {
        rectangle(self.color, [self.x, self.y, self.width, self.height],
                      context.transform,
                      graphics);
    }
    fn change_color(&mut self, color: [f32; 4])
    {
        self.color = color;
    }
    fn move_to(&mut self, x: f64, y: f64)
    {
        self.x = x;
        self.y = y;
    }
}