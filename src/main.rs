extern crate piston_window;
extern crate rand;

use piston_window::*;
use rand::Rng;

mod shape;

#[macro_use]
mod a_star;

use a_star::Point;
use std::collections::LinkedList;
use crate::shape::Mrectangle;
use crate::shape::Shape;

macro_rules! SCREEN {

    (ratio_to_maze) => (10);
    (width) => (MAZE!(width) * SCREEN!(ratio_to_maze));
    (height) => (MAZE!(height) * SCREEN!(ratio_to_maze));

    (path_color) => ([0.0, 1.0, 0.0, 1.0]);
    (wall_color) => ([0.0, 0.0, 0.0, 1.0]);
    (x_path_color) => ([1.0, 0.0, 0.0, 1.0]);
}

macro_rules! GAME {
    (start) => (Point::new(0.0, 0.0));
    (finish) => (Point::new((MAZE!(width) - 1) as f64, (MAZE!(height) - 1) as f64));
}

fn init_grid(grid: &mut [[u8;MAZE!(width)]; MAZE!(height)])
{
    let mut rng = rand::thread_rng();

    for (i, row) in grid.iter_mut().enumerate() {
        for (y, col) in row.iter_mut().enumerate() {

            if !(i == 0 && y == 0) && !(i == MAZE!(height) - 1 && y == MAZE!(width))
            {
                *col = rng.gen_range(0, 5);
            }  
        }
    }
    
}

fn main() {
    
    let start = GAME!(start);
    let finish = GAME!(finish);


    let mut maze = [[MAZE!(regular) as u8; MAZE!(width)]; MAZE!(height)];
    init_grid(&mut maze);

    let mut track = LinkedList::new();
    let mut blacklist = LinkedList::new();

    track.push_front(start.clone());

    let mut window: PistonWindow =
        WindowSettings::new("path", [SCREEN!(width), SCREEN!(height)])
        .exit_on_esc(true).build().unwrap();

    while let Some(event) = window.next() {
        window.draw_2d(&event, |context, mut graphics| {
            clear([1.0; 4], graphics);

            let next_point = a_star::get_next_point(&finish, &mut track, &mut blacklist, maze);
            maze[next_point.y as usize][next_point.x as usize] = if next_point == *track.front().unwrap() {
                                                                        MAZE!(path)
                                                                    }
                                                                    else
                                                                    {
                                                                        MAZE!(x_path)
                                                                    };
            
            let mut rect: Mrectangle = Shape::new([0.0, 0.0, 0.0 , 1.0]);
            rect.transform(SCREEN!(ratio_to_maze) as f64, SCREEN!(ratio_to_maze) as f64);
            rect.move_to(0.0, 0.0);
            
            for (i, row) in maze.iter_mut().enumerate() 
            {
                for (y, col) in row.iter_mut().enumerate()
                {
                    match *col
                    {
                        MAZE!(wall) => rect.change_color(SCREEN!(wall_color)),
                        MAZE!(path) => rect.change_color(SCREEN!(path_color)),
                        MAZE!(x_path) => rect.change_color(SCREEN!(x_path_color)),
                        _ => continue,
                    } 
                    rect.move_to((y * SCREEN!(ratio_to_maze)) as f64, (i * SCREEN!(ratio_to_maze)) as f64);
                    rect.get_shape(&event, &context, &mut graphics);
                }
            }
        });
    }
    
}
