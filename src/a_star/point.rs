
#[derive(PartialEq)]
pub struct Point
{
    pub x: f64,
    pub y: f64
}

impl Point
{
    pub fn new(x: f64, y: f64) -> Point
    {
        return Point{x, y};
    }

    pub fn distance(&self, other: &Point) -> f64
    {
        let x_dis = (*self).x - (*other).x;
        let y_dis = (*self).y - (*other).y;

        return (x_dis.powf(2.0) + y_dis.powf(2.0)).sqrt();
    }

    pub fn clone(&self) -> Point
    {
        return Point::new(self.x, self.y);
    }    
}