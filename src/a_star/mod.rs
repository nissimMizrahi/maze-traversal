use std::collections::LinkedList;

mod point;
pub use point::Point;

macro_rules! MAZE {
    (width) => (100);
    (height) => (60);

    (wall) => (0);
    (regular) => (1);
    (path) => (255);
    (x_path) => (127);

    (invalid_score) => (-1.0);
}

fn get_point_g(src: &Point, point: &Point, track: &LinkedList<Point>) -> f64
{
    let mut iter = track.iter();
    let last_point: &Point = match iter.next()
    {
        None => src,
        Some(x) => x,
    };

    let last_distance = track.len();
    let dis_to_last = point.distance(&last_point);

    if dis_to_last < 2.0 {
        return (last_distance + 1) as f64;
    }else{
        return MAZE!(invalid_score);
    };
}

fn get_total_score(src: &Point, dst: &Point, point: &Point, track: &LinkedList<Point>, field: [[u8; MAZE!(width)]; MAZE!(height)]) -> f64
{
    let g = get_point_g(src, point, &track);
    let h = point.distance(dst);

    if field[point.y as usize][point.x as usize] == 0 || g == MAZE!(invalid_score)
    {
        return MAZE!(invalid_score);
    }
    else
    {
        return g + h;
    }
}

pub fn get_value(src: &Point, 
                dst: &Point, 
                track: &LinkedList<Point>, 
                blacklist: &LinkedList<Point>, 
                field: [[u8; MAZE!(width)]; MAZE!(height)])
                -> Point
{
    let mut scores = Vec::new();

    let mut iter = track.iter();
    let last_point: &Point = match iter.next()
    {
        None => src,
        Some(x) => x,
    };


    let rigth = if last_point.x >= (MAZE!(width) - 1) as f64 {0.0} else {1.0};
    let up = if last_point.y <= 0.0 {0.0} else {1.0};
    let left = if last_point.x <= 0.0 {0.0} else {1.0};
    let down = if last_point.y >= (MAZE!(height) - 1) as f64 {0.0} else {1.0};


    scores.push(Point::new(last_point.x + rigth, last_point.y));
    scores.push(Point::new(last_point.x - left, last_point.y));

    scores.push(Point::new(last_point.x, last_point.y + down));
    scores.push(Point::new(last_point.x, last_point.y - up));

    let mut ret_point = last_point;

    for point in scores.iter()
    {
        let point_score = get_total_score(src, dst, point, &track, field);

        if point_score == MAZE!(invalid_score) || point == last_point || blacklist.contains(point)
        {
            continue;
        }
        else if point_score < get_total_score(src, dst, ret_point, &track, field)
        {
            ret_point = point;
        }
    }


    return ret_point.clone();
}

pub fn get_next_point(dst: &Point,
                    track: &mut LinkedList<Point>,
                    blacklist: &mut LinkedList<Point>, 
                    maze: [[u8; MAZE!(width)]; MAZE!(height)])
                    -> Point
{
    if *track.front().unwrap() != *dst
    {
        let next_point = get_value(track.front().unwrap(), dst, &track, &blacklist, maze);
        
        if !track.is_empty()
        {
            if next_point != *track.front().unwrap()
            {
                track.push_front(next_point.clone());
            }
            else
            {
                blacklist.push_front(track.pop_front().unwrap());
            }
        }
        return next_point;
    }
    else
    {
        return dst.clone();
    }
    
}